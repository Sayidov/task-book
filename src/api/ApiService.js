import request from '@/utils/axios'


const ApiService = {


    getBooks(keyWord){
        return request.get(`https://www.googleapis.com/books/v1/volumes?q=${keyWord}`)
    }

}
export default  ApiService
